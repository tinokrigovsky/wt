let nextArtIndex = 1;

let inputs = document.getElementById("Form").elements;

let opinions = [];

const opinionsElm = document.getElementById("opinionsContainer");
if (localStorage.judoReviews) {
    opinions = JSON.parse(localStorage.judoReviews);
}
opinionsElm.innerHTML = opinionArray2html(opinions);
console.log(opinions);

/*function addArticle() {
    //inputy
    inputs = document.getElementById("Form").elements;
        // tvorba elementov
        let newArt = document.createElement("div");
        let cardImage = document.createElement("IMG");
        let cardBody = document.createElement("div");
        let title = document.createElement("h5");
        let paragraph = document.createElement("p");
        //stylizacia
        newArt.className = "card mb-4";
        cardImage.className = "card-img-top";
        cardBody.className = "card-body";
        title.className = "card-title";
        paragraph.className = "card-text";
        //pridelovanie hodnot
        title.innerHTML = inputs[0].value;
        cardImage.src = inputs[2].value;
        cardImage.alt = "reviewImage";
        paragraph.innerHTML = inputs[6].value;
    if(inputs[0].value==="" || inputs[6].value===""|| inputs[1].value===""){
        return false;
    }
        //vnaranie elementov
        if (cardImage.getAttribute('src') !== "") {
            newArt.appendChild(cardImage);
        }
        cardBody.appendChild(title);
        cardBody.appendChild(paragraph);
        newArt.appendChild(cardBody);
        //pridanie celeho elementu
        document.getElementById("container").insertBefore(newArt, document.getElementById("endArts"));
        newArt.setAttribute("id", "cl" + nextArtIndex);
        nextArtIndex++;
        return false;
}*/



let myFrmElm = document.getElementById("Form");

myFrmElm.addEventListener
("submit", processOpnFrmData);

function processOpnFrmData(event) {
    //1.prevent normal event (form sending) processing
    event.preventDefault();

    //2. Read and adjust data from the form (here we remove white spaces before and after the strings)
    const nopName = inputs[0].value.trim();
    const nopMail = inputs[1].value.trim();
    const nopReviewImage = inputs[2].value.trim();
    const nopMan = inputs[3].checked;
    const nopCheckbox = inputs[5].checked;
    const nopOpn = inputs[6].value.trim();

    //3. Verify the data
    if (nopName === "" || nopOpn === "") {
        window.alert("Please, enter both your name and opinion");
        return;
    }
    //3. Add the data to the array opinions and local storage
    const newOpinion =
        {
            name: nopName,
            comment: nopOpn,
            mail: nopMail,
            isMan: nopMan,
            iTrained: nopCheckbox,
            reviewImage: nopReviewImage,
            created: new Date()
        };

    console.log("New opinion:\n " + JSON.stringify(newOpinion));

    opinions.push(newOpinion);

    localStorage.judoReviews = JSON.stringify(opinions);

    opinionsElm.innerHTML += opinion2html(newOpinion);
    //4. Notify the user
    window.alert("Your opinion has been stored. Look to the console");
    console.log("New opinion added");
    console.log(opinions);

    //5. Reset the form
    myFrmElm.reset(); //resets the form
    myFrmElm.classList.remove('was-validated')
}


function opinion2html(opinion) {

    opinion.createdDate = (new Date(opinion.created)).toDateString();
    opinion.willReturnMessage = opinion.willReturn ? "Bol som na treningu" : "nebol som na treningu";

    const template = document.getElementById("mTmplOneOpinion").innerHTML;
    const htmlWOp = Mustache.render(template, opinion);

    delete (opinion.createdDate);
    delete (opinion.willReturnMessage);

    return htmlWOp;
}

function opinionArray2html(sourceData) {
    return sourceData.reduce((htmlWithOpinions, opn) => htmlWithOpinions + opinion2html(opn), "");

}


